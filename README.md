Wi-Fi Watchdog
==============
So, I've bought a cheap Android tablet for my family. And it turned out, it has
some problem with our Wi-Fi. The connection seems ok, but no data was passing
through sometimes. The only workaround we figured out is to restart the Wi-Fi on
the tablet. And that's exactly what this app does.

How does it work?
-----------------
You specify an URL to check every *n* minutes
([AlarmManager](https://developer.android.com/reference/android/app/AlarmManager.html)
doesn't allow you less than a minute) with HTTP `HEAD` request. If it can't
resolve the given URL, or the request times out (after configurable number of
seconds), it restarts the Wi-Fi.

The setting are *live*, whenever you change something, it immediately affects
the watchdog. The exception is the check interval, you have to restart the
watchdog for that to take effect.

FAQ
---
Got questions? Just send me an email.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-2.png)](http://www.wtfpl.net)