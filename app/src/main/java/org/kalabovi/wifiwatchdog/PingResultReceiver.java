package org.kalabovi.wifiwatchdog;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.Nullable;

public class PingResultReceiver extends ResultReceiver {

    private Receiver receiver;

    public PingResultReceiver(final Handler handler) {
        super(handler);
    }

    public interface Receiver {
        void onReceiveResult(final int resultCode, final @Nullable Bundle resultData);
    }

    public void setReceiver(final Receiver receiver) {
        this.receiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, @Nullable final Bundle resultData) {
        if (receiver != null) {
            receiver.onReceiveResult(resultCode, resultData);
        }
    }
}