package org.kalabovi.wifiwatchdog;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class PingIntentService extends IntentService {
    private static final String TAG = IntentService.class.getSimpleName();

    public static final String KEY_URL = "url";
    public static final String KEY_RECEIVER = "receiver";
    public static final String KEY_TIMEOUT = "timeout";
    public static final String KEY_INTERVAL = "interval";
    public static final String KEY_MESSAGE = "msg";

    public PingIntentService() {
        super("PingIntentService");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        final ResultReceiver receiver = intent.getParcelableExtra(KEY_RECEIVER);
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        HttpURLConnection http = null;
        try {
            final URL url = new URL(prefs.getString(KEY_URL, null));
            http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("HEAD");
            final int timeout = (int) TimeUnit.SECONDS.toMillis(prefs.getInt(KEY_TIMEOUT, 1));
            http.setConnectTimeout(timeout);
            http.setReadTimeout(timeout);
            final int responseCode = http.getResponseCode();
            Log.d(TAG, String.valueOf(responseCode));
            if (responseCode > 0) {
                receiver.send(1, null);
            } else {
                receiver.send(0, null);
            }
        } catch (final SocketTimeoutException e) {
            Log.w(TAG, "Timeout!");
            receiver.send(0, null);
        } catch (final IOException e) {
            final Bundle bundle = new Bundle();
            bundle.putString(KEY_MESSAGE, e.getMessage());
            receiver.send(0, bundle);
        } finally {
            if (http != null) {
                http.disconnect();
                http = null;
            }
        }
    }
}
