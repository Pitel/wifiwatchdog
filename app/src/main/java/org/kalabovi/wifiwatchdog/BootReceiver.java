package org.kalabovi.wifiwatchdog;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import java.util.concurrent.TimeUnit;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
            final AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            final PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, new Intent(context, PingBroadcastReceiver.class), 0);
            am.cancel(alarmIntent);
            am.setInexactRepeating(
                    AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime(),
                    TimeUnit.MINUTES.toMillis(PreferenceManager.getDefaultSharedPreferences(context).getInt(PingIntentService.KEY_INTERVAL, 1)),
                    alarmIntent
            );
        }
    }
}
