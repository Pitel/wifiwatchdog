package org.kalabovi.wifiwatchdog;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements PingResultReceiver.Receiver {

    private PingResultReceiver receiver;
    private SharedPreferences prefs;

    @Bind(R.id.url)
    EditText urlText;
    @Bind(R.id.interval)
    EditText intervalText;
    @Bind(R.id.timeout)
    EditText timeoutText;
    @Bind(R.id.go)
    FloatingActionButton go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        receiver = new PingResultReceiver(new Handler());
        receiver.setReceiver(this);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        urlText.setText(prefs.getString(PingIntentService.KEY_URL, null));
        urlText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
            }

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                prefs.edit().putString(PingIntentService.KEY_URL, s.toString()).apply();
            }
        });

        timeoutText.setText(String.valueOf(prefs.getInt(PingIntentService.KEY_TIMEOUT, 1)));
        timeoutText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
            }

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (s.length() > 0) {
                    prefs.edit().putInt(PingIntentService.KEY_TIMEOUT, Integer.parseInt(s.toString())).apply();
                }
            }
        });

        intervalText.setText(String.valueOf(prefs.getInt(PingIntentService.KEY_INTERVAL, 1)));
        intervalText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
            }

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                if (s.length() > 0) {
                    prefs.edit().putInt(PingIntentService.KEY_INTERVAL, Integer.parseInt(s.toString())).apply();
                }
            }
        });

        final ComponentName receiver = new ComponentName(this, BootReceiver.class);
        final PackageManager pm = getPackageManager();

        switch (pm.getComponentEnabledSetting(receiver)) {
            case PackageManager.COMPONENT_ENABLED_STATE_ENABLED:
                go.setImageResource(android.R.drawable.ic_media_pause);
                break;
            case PackageManager.COMPONENT_ENABLED_STATE_DISABLED:
            case PackageManager.COMPONENT_ENABLED_STATE_DEFAULT:
                go.setImageResource(android.R.drawable.ic_media_play);
                break;
        }
    }

    @OnClick(R.id.test)
    void ping() {
        final Intent intent = new Intent(this, PingIntentService.class);
        intent.putExtra(PingIntentService.KEY_RECEIVER, receiver);
        startService(intent);
    }

    @Override
    public void onReceiveResult(final int resultCode, @Nullable final Bundle resultData) {
        if (resultData != null && resultData.containsKey(PingIntentService.KEY_MESSAGE)) {
            Toast.makeText(this, resultData.getString(PingIntentService.KEY_MESSAGE), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, resultCode != 0 ? ":-)" : ":-(", Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.go)
    void go() {
        final AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
        final PendingIntent intent = PendingIntent.getBroadcast(this, 0, new Intent(this, PingBroadcastReceiver.class), 0);

        final ComponentName receiver = new ComponentName(this, BootReceiver.class);
        final PackageManager pm = getPackageManager();

        switch (pm.getComponentEnabledSetting(receiver)) {
            case PackageManager.COMPONENT_ENABLED_STATE_ENABLED:
                pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                am.cancel(intent);
                go.setImageResource(android.R.drawable.ic_media_play);
                break;
            case PackageManager.COMPONENT_ENABLED_STATE_DISABLED:
            case PackageManager.COMPONENT_ENABLED_STATE_DEFAULT:
                pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                am.cancel(intent);
                am.setInexactRepeating(
                        AlarmManager.ELAPSED_REALTIME,
                        SystemClock.elapsedRealtime(),
                        TimeUnit.MINUTES.toMillis(prefs.getInt(PingIntentService.KEY_INTERVAL, 1)),
                        intent
                );
                go.setImageResource(android.R.drawable.ic_media_pause);
                break;
        }
    }
}
