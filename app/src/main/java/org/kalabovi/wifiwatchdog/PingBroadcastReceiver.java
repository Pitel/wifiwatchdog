package org.kalabovi.wifiwatchdog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

public class PingBroadcastReceiver extends BroadcastReceiver implements PingResultReceiver.Receiver {
    private static final String TAG = PingBroadcastReceiver.class.getSimpleName();
    private Context context;

    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.d(TAG, "Alarm!");
        this.context = context;
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected() && ni.getType() == ConnectivityManager.TYPE_WIFI) {
            Log.d(TAG, "Wi-Fi, checking connection");
            final PingResultReceiver receiver = new PingResultReceiver(new Handler());
            receiver.setReceiver(this);
            final Intent serviceIntent = new Intent(context, PingIntentService.class);
            serviceIntent.putExtra(PingIntentService.KEY_RECEIVER, receiver);
            context.startService(serviceIntent);
        } else {
            Log.d(TAG, "No Wi-Fi, doing nothing.");
        }
    }

    @Override
    public void onReceiveResult(final int resultCode, final @Nullable Bundle resultData) {
        if (resultCode == 0) {
            Log.w(TAG, "Wi-Fi is broken!");
            Toast.makeText(context, "Restarting Wi-Fi!", Toast.LENGTH_SHORT).show();
            final WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            wm.setWifiEnabled(false);
            wm.setWifiEnabled(true);
        } else {
            Log.i(TAG, "Wi-Fi is ok.");
        }
    }
}
